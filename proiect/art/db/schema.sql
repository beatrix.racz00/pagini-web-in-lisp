CREATE TABLE pictor (
   ID INTEGER PRIMARY KEY NOT NULL,
   name TEXT NOT NULL 
 );
 
 CREATE TABLE painting (
  ID INTEGER PRIMARY KEY NOT NULL,
  image BLOB NOT NULL,
  name TEXT NOT NULL,
  year INTEGER NOT NULL,
  style TEXT NOT NULL,
  IDPainter INTEGER NOT NULL
   CONSTRAINT picID FOREIGN KEY (IDPainter) REFERENCES pictor(ID)
) 

INSERT INTO pictor (ID, name) VALUES(1, 'Leonardo da Vinci');
INSERT INTO pictor (ID, name) VALUES(2, 'Johannes Vermeer');
INSERT INTO pictor (ID, name) VALUES(3, 'Michelangelo Buonarotti ');
INSERT INTO pictor (ID, name) VALUES(4, 'Rembrandt Harmenszoon van Rijn');
INSERT INTO pictor (ID, name) VALUES(5, 'Pablo Picasso');
INSERT INTO pictor (ID, name) VALUES(6, 'Claude Monet');
INSERT INTO pictor (ID, name) VALUES(7, 'Vincent van Gogh');
