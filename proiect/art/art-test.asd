(defsystem "art-test"
  :defsystem-depends-on ("prove-asdf")
  :author "Beatrix"
  :license ""
  :depends-on ("art"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "art"))))
  :description "Test system for art"
  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
