(defpackage :calendar
  (:use :cl :cl-who :hunchentoot :parenscript :cl-mongo :cl-pass ))
;;(ql:quickload '(cl-who hunchentoot parenscript cl-mongo cl-pass local-time ))
(in-package :calendar)

(defvar usid "none")

(defun setid (email)
(setq usid email))

(defclass task ()
  ((name  :reader   name
          :initarg  :name)
	(description  :accessor   description
          :initarg  :description)
	(day  :accessor   day
          :initarg  :day)
	(timp  :accessor   timp
          :initarg  :timp)
    (invited  :accessor invited
          :initarg  :invited)
   (userid :accessor userid
          :initarg :userid
          :initform 0)))
		  
(defclass user()
	((firstname :reader firstname
		   :initarg :firstname)
     (lastname :reader lastname
		   :initarg :lastname)
     (email :reader email
		 :initarg :email)
	 (password :reader password
		 :initarg :password)))

(defmethod print-object ((object task) stream)
  (print-unreadable-object (object stream :type t)
    (with-slots (name userid) object
      (format stream "name: ~s with ~d userid" name userid))))



(cl-mongo:db.use "tasks")

(defparameter *task-collection* "task")
(defparameter *user-collection* "user")


(defun task->doc (task)
  ($ ($ "name" (name task))
	 ($ "description" (description task))
     ($ "day" (day task))
	 ($ "timp" (timp task))
	 ($ "invited" (invited task))
     ($ "userid" (userid task))))

(defun doc->task (task-doc)
  (make-instance 'task :name (get-element "name" task-doc)
					   :description (get-element "description" task-doc)
					   :day (get-element "day" task-doc)
					   :timp (get-element "timp" task-doc)
					   :invited (get-element "invited" task-doc)
                       :userid (get-element "userid" task-doc)))
(defun user->doc (user)
  ($ ($ "firstname" (firstname user))
	 ($ "lastname" (lastname user))
     ($ "email" (email user))
	 ($ "password" (password user))))

(defun doc->user (user-doc)
  (make-instance 'user :firstname (get-element "firstname" user-doc)
					   :lastname (get-element "lastname" user-doc)
					   :email (get-element "email" user-doc)
					   :password (get-element "password" user-doc)))


(defun task-from-name (name)
  (let ((found-tasks (docs (db.find *task-collection* ($ "name" name) ))))
    (when found-tasks
      (doc->task (first found-tasks)))))
	  

(defun task-stored? (name)
  (task-from-name name))

(defun tasks ()
  (mapcar #'doc->task
          (docs (iter (db.sort *task-collection* :all
                                                 :field "timp"
                                                 :asc nil)))))

(defun unique-index-on (field)
  (db.ensure-index *user-collection*
                   ($ field 1)
                   :unique t))

(unique-index-on "email")



(defmethod update-task  (task name description day timp invited)
(set (name task) name)
(set (description task) description)
(set (day task) day)
(set (timp task) timp)
(set (invited task) invited)
)

(defmethod update-task :after (task name description day timp invited)
(let ((task-doc (task->doc task)))
    (db.update *task-collection* ($ "name" (name task)) task-doc)))

(defmethod del-task  (task name)

    (db.delete *task-collection* ($ "name" (name task))))



(defun add-task (name description day timp invited userid)
  (let ((task (make-instance 'task :name name :description description :day day :timp timp :invited invited :userid userid)))
    (db.insert *task-collection* (task->doc task))))


(defun create-user (firstname lastname email password)
  "create a new user in database"
  (let ((user (make-instance 'user :firstname firstname :lastname lastname :email email :password password)))
    (db.insert *user-collection* (user->doc user))))
	
	
;;cautam mail-ul in db si verificam daca parola este corecta
	
(defun user-exist (email pass)
  (let ((found-users (docs (db.find *user-collection* (kv ($ "email" email) ($ "password" pass)) ))))
	(when found-users
      (doc->user (first found-users)))
	  ))

(defun login-user (email password)
  (user-exist email password))


(defun mail-exist (email)
  (let ((found-users (docs (db.find *user-collection* ($ "email" email) ))))
    (when found-users
      (doc->user (first found-users)))))

(defun check-user (email)
  (mail-exist email))

;; Web Server - Hunchentoot

(defun start-server (port)
  (start (make-instance 'easy-acceptor :port port)))


;;fisiere css si poze
(defun publish-static-content ()
  (push (create-static-file-dispatcher-and-handler
         "/logo.jpg" "static/logo.jpg") *dispatch-table*)
  (push (create-static-file-dispatcher-and-handler
         "/tasky.css" "static/tasky.css") *dispatch-table*))
 

(defun get-calendar-rows (month year)
  (let* ((days-in-month (local-time:days-in-month month year))
         (first-day (parse-integer (local-time:format-timestring nil (local-time:encode-timestamp 0 0 0 0 1 month year) :format (list :weekday))))
         (first-week-days (- 7 first-day))
         (remaining-days (- days-in-month first-week-days))
         (rows-needed (ceiling (/ remaining-days 7)))
         (calendar (make-array (1+ rows-needed) :initial-element (make-array 7 :initial-element ""))))

    ;; populate first week
    (let ((first-row (make-array 7 :initial-element "")))
      (loop for j from first-day to 6
            for x from 1 to 7
            do (setf (aref first-row j) x))
      (setf (aref calendar 0) first-row))

    ;; populate the remaining rows
    (loop for i from 1 to rows-needed 
          do(let ((temp (make-array 7 :initial-element "")))
              (loop for j from 0 to 6
                    do(let ((dt (+ first-week-days (* 7 (- i 1)) (+ j 1))))
                        (if (<= dt days-in-month)
                            (setf (aref temp j) dt)
                            (setf (aref temp j) ""))))
              (setf (aref calendar i) temp)))
    calendar))

(defun get-current-month ()
  (parse-integer (local-time:format-timestring nil (local-time:now) :format (list :month))))

(defun get-current-year ()
  (parse-integer (local-time:format-timestring nil (local-time:now) :format (list :year))))

(defun get-current-date ()
  (parse-integer (local-time:format-timestring nil (local-time:now) :format (list :day))))
  



(defvar month (get-current-month))
(defvar dayz (get-current-date))
(defvar year (get-current-year))
(defvar rows (get-calendar-rows month year))

(defvar thisday (concatenate 'string (write-to-string year) "-0" (write-to-string month) "-" (write-to-string dayz)))

(defun next () 
(if (= month 12)
 (progn 
(setf month 1)(incf year))
(incf month)
)
(setq rows (get-calendar-rows month year))
)

(defun previous () 
(if (= month 1)
 (progn (setf month 12) (decf year))
(decf month)
)
(setq rows (get-calendar-rows month year))
)

;;pagini web
(setf (html-mode) :html5)




;un schelete pentru fiecare pagina pe care o vom folosi
(defmacro standard-page ((&key title script) &body body)
  `(with-html-output-to-string
    (*standard-output* nil :prologue t :indent t)
    (:html :lang "en"
           (:head
            (:meta :charset "utf-8")
            (:title ,title)
            (:link :type "text/css"
                   :rel "stylesheet"
                   :href "/tasky.css")
			(:link :href "https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
					:crossorigin "anonymous"
                   :rel "stylesheet"
                   :integrity "sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We")
            ,(when script
               `(:script :type "text/javascript"
                         (str ,script))))
           (:body
            (:div 
				  (:ul 
				  (:li (:a :href "calendar" "Homepage"))
				  (:li (:a :href (format nil "lista?mail=~a" (url-encode "")) "List of tasks"))
				  (:li (:a :href (format nil "new-task?selected-day=~a" (url-encode thisday)) "Add a new task"))
				  (:img :src "/logo.jpg"
                        :alt "logo.jpg"
                        :class "logo"))
				 
				  )
            ,@body))))


(defmacro login-page ((&key title script) &body body)
  `(with-html-output-to-string
    (*standard-output* nil :prologue t :indent t)
    (:html :lang "en"
           (:head
            (:meta :charset "utf-8")
            (:title ,title)
            (:link :type "text/css"
                   :rel "stylesheet"
                   :href "/tasky.css")
            ,(when script
               `(:script :type "text/javascript"
                         (str ,script))))
           (:body
		   (:div ;;(:img :src "/logo.jpg"
                    ;;    :alt "logo.jpg"
                      ;;  :class "logo")
				  
				 
				  )
            
            ,@body))))

;;pagina lista cu toate taskurile pe care le are user-ul
(define-easy-handler (lista :uri "/lista") (mail)
  (standard-page (:title "List of tasks")
    (:form :action "/lista" :method "post" :id "formy"
			(:p  
                (:input :type "email" :name "mail" :class "mail" :placeholder "searching for an event with  certain person?")
             (:input :type "submit" :value "search" :class "butn")))
	
	(:div :class "charta" 
    (:h1 "Events created by you:")
	(dolist (task (tasks))
	(if  (string= mail "")
		(if (string= (userid task) usid)
			(htm
				(:div :class "boxa" (fmt "~A" (escape-string (name task)))
				(:p :class "afdate" (fmt "~A" (escape-string (day task))))
				(:a :href (format nil "show-task?title=~a" (url-encode (name task)))  (:span :class "linky")))))
		(if (string= (invited task) mail)
			(if (string= (userid task) usid)
			(htm
				(:div :class "boxa" (fmt "~A" (escape-string (name task)))
				(:p :class "afdate" (fmt "~A" (escape-string (day task))))
				(:a :href (format nil "show-task?title=~a" (url-encode (name task)))  (:span :class "linky")))))
))))
	(:div :class "charta" 
    (:h1 "Events you are invited to:")
	(dolist (task (tasks))
	(if  (string= mail "")
	(if (string= (invited task) usid)
	 (htm
	  (:div :class "boxb" 
	  (fmt "~A " (escape-string (name task)))
	  (:p :class "afdate" (fmt "~A" (escape-string (day task))))
	  (:p (fmt "Invited by ~a " (userid task)))
		(:a :href (format nil "show-task?title=~a" (url-encode 
                                                     (name task)))  (:span :class "linky"))
		)))
		(if (string= (userid task) mail)
	(if (string= (invited task) usid)
	 (htm
	  (:div :class "boxb" 
	  (fmt "~A " (escape-string (name task)))
	  (:p :class "afdate" (fmt "~A" (escape-string (day task))))
	  (:p (fmt "Invited by ~a " (userid task)))
		(:a :href (format nil "show-task?title=~a" (url-encode 
                                                     (name task)))  (:span :class "linky"))
		)))	
		))
		
	))))
									   
(define-easy-handler (show-task :uri "/show-task") (title)
	(let ((this-task (task-from-name title) )) 
		(standard-page (:title "Details")
			
			(:div :class "afisare"
			
			(if (string= usid (invited this-task))
				(htm (:p :class "afisare" (:h3 (fmt "you were invited by ~a to:" (escape-string (userid this-task))))))
			)
			(:h3 (fmt "~A" (escape-string (name this-task))))
			(:p :class "afisez" (fmt "Description: ~A" (escape-string (description this-task))))
			(:p :class "afisez" (fmt "Date and time: ~A from ~a" (escape-string (day this-task))(escape-string (timp this-task))))
			(if (string= usid (userid this-task))
				(htm (:p (fmt "You invited: ~A" (escape-string (invited this-task))))
				(:p :id "editare" (:a :href (format nil "edit-task?name=~a" (url-encode 
                                                     (name this-task)))  "Edit" ))
					(:p :id "stergere" (:a :href (format nil "delete-task?name=~a" (url-encode 
                                                     (name this-task)))  "Delete" )))
			)
			)
	)))							   

(define-easy-handler (edit-task :uri "/edit-task") (name)
	(let ((task (task-from-name name) ))
  (standard-page (:title "/edit task")
   
     (:form :action "/task-edited" :method "post" :id "addform"
            (:p "Task name" (:br)
                (:input :type "text" :name "name" :class "txt" :value (name task)))
			(:p "Description" (:br)
                (:input :type "text" :name "description" :class "txt" :value (description task)))
			(:p "Day" (:br)
                (:input :type "date" :name "day" :class "txt" :value (day task)))
			(:p "Time" (:br)
                (:input :type "time" :name "timp" :class "txt" :value (timp task)))
			(:p "Who is invited?" (:br)
                (:input :type "email" :name "invited" :class "txt" :value (invited task)))
            (:p (:input :type "submit" :value "Edit" :class "btn"))))))

(define-easy-handler (task-edited :uri "/task-edited") (name description day timp invited)
	(when (task-stored? name)
  (update-task (task-from-name name) name description day timp invited ))
  (redirect "/calendar"))
  
  (define-easy-handler (delete-task :uri "/delete-task") (name)
	(when (task-stored? name)
  (del-task (task-from-name name) name))
  (redirect "/calendar"))
									   
(define-easy-handler (nex :uri "/nex") ()
  (next)
  (redirect "/calendar"))
	
	(define-easy-handler (prev :uri "/prev") ()
  (previous)
  (redirect "/calendar"))
	
	(defvar answer "no")
	(defvar zi "")
	(defvar luna "")
	
(define-easy-handler (calendar :uri "/calendar") ()
  (standard-page (:title "Homepage")
	   (:form :action "/prev" :method "post"  :class "buttons"
	   (:p (:input :type "submit" :value "Previous" :class "left"))
	   )
	   (:div :class "mid" (:h1  (fmt "Calendar: ~a ~a" month year)))
	   (:form :action "/nex" :method "post" :class "buttons"
	   (:p (:input :type "submit" :value "Next" :class "left")))  
	 (:table :class "table table-bordered mt-2" :style "margin-top:30px;"
		(:thead :class "table-dark"
			(:tr :class "text-center"
				(:th "Sunday")
				(:th "Monday")
				(:th "Tuesday")
				(:th "Wednesday")
				(:th "Thursday")
				(:th "Friday")
				(:th "Saturday")))
		(:tbody 
		 (dotimes (x 5)
		  (htm (:tr 
		  (dotimes (y 7)
		  (htm
			(:td :class "tab" (fmt "~a" (aref (aref rows x) y))
			(if (numberp (aref (aref rows x) y))
			(if (>= (aref (aref rows x) y) 10)
				(setq zi (write-to-string (aref (aref rows x) y) ))
				(setq zi (concatenate 'string "0" (write-to-string (aref (aref rows x) y) ))))
			)
			(if (>= month 10)
				(setq luna (write-to-string month ))
				(setq luna (concatenate 'string "0" (write-to-string month ))))
				
			(let ((today (concatenate 'string  (write-to-string year)  "-"  luna "-" zi )))
			(setq answer "no")
			(dolist (task(tasks))
					(if (string= (day task) today)
						(progn
							(if (string= (userid task) usid)
								(htm (:div :class "tichete" (fmt "~a" (name task))
								(:a :href (format nil "show-task?title=~a" (url-encode 
                                                     (name task)))  (:span :class "linky")))))
							(if (string= (invited task) usid)
								(htm (:div :class "invitatii" (fmt "~a" (name task))
								(:a :href (format nil "show-task?title=~a" (url-encode 
                                                     (name task)))  (:span :class "linky")))))
							(setq answer "yes")
						)
						)
					)
					(if (string= answer "no")
						(htm (:a :href (format nil "new-task?selected-day=~a" (url-encode today))  (:span :class "linky")))
					))))))))))))
									   
	


;pagina creare task nou
(define-easy-handler (new-task :uri "/new-task") (selected-day)
  (standard-page (:title "Add a new task"
                         :script (ps  
                                  (defvar add-form nil)
                                  (defun validate-task-name (evt)
                                    (when (= (@ add-form name value) "")
                                      (chain evt (prevent-default))
                                      (alert "Please enter a name.")))
                                  (defun init ()
                                    (setf add-form (chain document
                                                          (get-element-by-id "addform")))
                                    (chain add-form
                                           (add-event-listener "submit" validate-task-name false)))
                                  (setf (chain window onload) init)))
   
     (:form :action "/task-added" :method "post" :id "addform"
            (:p "What is the name of the task?" (:br)
                (:input :type "text" :name "name" :class "txt"))
			(:p "description" (:br)
                (:input :type "text" :name "description" :class "txt"))
			(:p "day" (:br)
                (:input :type "date" :name "day" :class "txt" :value selected-day))
			(:p "timp" (:br)
                (:input :type "time" :name "timp" :class "txt"))
			(:p "invited" (:br)
                (:input :type "email" :name "invited" :class "txt"))
            (:p (:input :type "submit" :value "Add" :class "btn")))))

(define-easy-handler (task-added :uri "/task-added") (name description day timp invited )
  (unless (or (null name) (zerop (length name)))
    (add-task name description day timp invited usid))
  (redirect "/calendar"))

 
 ;;pagina inregistrare
(define-easy-handler (register :uri "/register") ()
  (standard-page (:title "Register now")
     (:form :action "/registered" :method "post" :id "addform"
            (:p "Your firstname:" (:br)
                (:input :type "text" :name "firstname" :class "txt"))
			(:p "Lastname" (:br)
                (:input :type "text" :name "lastname" :class "txt"))
			(:p "Email address" (:br)
                (:input :type "email" :name "email" :class "txt"))
			(:p "Password" (:br)
                (:input :type "password" :name "password" :class "txt"))
			(:p ( :a :href "/" "Already a member?" ))
            (:p (:input :type "submit" :value "Register" :class "btn")))))
			
			
(define-easy-handler (registered :uri "/registered") (firstname lastname email password)
  (unless (or (null firstname) (zerop (length firstname)) (check-user email))
   (progn
    (create-user firstname lastname email password)
	(setid email)))
  (redirect "/calendar" ))



(define-easy-handler (login :uri "/") ()
  (login-page (:title "Login easily"
					:script (ps  
                                  (defvar add-form nil)
                                  (defun validate-user-name (evt)
                                    (when (= (@ add-form email value) "")
                                      (chain evt (prevent-default))
                                      (alert "Please enter the correct email and password.")))
                                  (defun init ()
                                    (setf add-form (chain document
                                                          (get-element-by-id "addform")))
                                    (chain add-form
                                           (add-event-listener "submit" validate-user-name false)))
                                  (setf (chain window onload) init)))
     (:h1 "Login")
     (:form :action "/logged" :method "post" :id "addform"
			(:p "Email address" (:br)
                (:input :type "email" :name "email" :class "txt"))
			(:p "Password" (:br)
                (:input :type "password" :name "password" :class "txt"))
			(:p ( :a :href "/register" "Not a member yet?" ))
            (:p (:input :type "submit" :value "Login" :class "btn")))))





(define-easy-handler (logged :uri "/logged") (email password)
     (if (login-user email password)
	  (progn (setid email)
	(redirect "/calendar") )
	(redirect "/") 
	))			

(publish-static-content)
(start-server 8080)